/**
 * Created by Vladislav_Danilov on 06.12.16.
 */

var tree = [
    {
        name: "computer",
        parent: "catalog"
    },
    {
        name: "notebook",
        parent: "computer"
    },
    {
        name: "15inch",
        parent: "notebook"
    },
    {
        name: "desktop",
        parent: "computer"
    }
];

window.onload = function () {
    tree.sort(function (a, b) {
        var nameA = a.name.toUpperCase();
        var nameB = b.name.toUpperCase();
        if (nameA < nameB) {
            return -1;
        }
        if (nameA > nameB) {
            return 1;
        }
        return 0;
    });
    arrayRead("catalog", "")
}

function arrayRead(rootElement, whiteSpace) {
    console.log(whiteSpace + rootElement)
    for (var i = 0; i < tree.length; i++) {
        if (tree[i].parent == rootElement) {
            arrayRead(tree[i].name, whiteSpace + " ")
        }
    }
}